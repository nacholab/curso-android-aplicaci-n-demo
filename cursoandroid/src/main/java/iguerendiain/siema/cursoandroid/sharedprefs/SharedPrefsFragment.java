package iguerendiain.siema.cursoandroid.sharedprefs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import iguerendiain.siema.cursoandroid.R;

/**
 * Created by capitannerd on 18/03/14.
 */
public class SharedPrefsFragment extends Fragment {
    private static final String SP_NAME = "CURSO_ANDROID_SP";

    private static final String SPKEY_NAME = "name";
    private static final String SPKEY_LASTNAME = "lastname";
    private static final String SPKEY_AGE = "age";
    private static final String SPKEY_SEX = "sex";

    private static final int MALE = 0;
    private static final int FEMALE = 1;

    private EditText name;
    private EditText lastname;
    private EditText age;
    private RadioGroup sex;
    private RadioButton female;
    private RadioButton male;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sharedprefs, container, false);

        this.name = (EditText) rootView.findViewById(R.id.name_input);
        this.lastname = (EditText) rootView.findViewById(R.id.lastname_input);
        this.age = (EditText) rootView.findViewById(R.id.age_input);
        this.sex = (RadioGroup) rootView.findViewById(R.id.sex_input);
        this.female = (RadioButton) rootView.findViewById(R.id.sex_input_female);
        this.male = (RadioButton) rootView.findViewById(R.id.sex_input_male);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.restoreData();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.storeData();
    }

    private void restoreData(){
        SharedPreferences form = this.getActivity().getSharedPreferences(SP_NAME, 0);

        String nameValue = form.getString(SPKEY_NAME,"");
        String lastnameValue = form.getString(SPKEY_LASTNAME,"");
        int ageValue = form.getInt(SPKEY_AGE,-1);
        int sexValue = form.getInt(SPKEY_SEX,FEMALE);

        this.name.setText(nameValue);
        this.lastname.setText(lastnameValue);
        if (ageValue<0) {
            this.age.setText("");
        }else{
            this.age.setText(Integer.toString(ageValue));
        }

        switch (sexValue){
            case FEMALE:
                this.female.setChecked(true);
                this.male.setChecked(false);
            break;
            case MALE:
                this.female.setChecked(false);
                this.male.setChecked(true);
            break;
        }
    }

    private void storeData(){
        String nameValue = this.name.getText().toString();
        String lastnameValue = this.lastname.getText().toString();
        int ageValue;

        try {
            ageValue = Integer.valueOf(this.age.getText().toString());
        }catch(NumberFormatException e){
            ageValue = -1;
        }

        int sexValue;
        switch (this.sex.getCheckedRadioButtonId()){
            case R.id.sex_input_female:
                sexValue = FEMALE;
            break;
            case R.id.sex_input_male:
                sexValue = MALE;
            break;
            default:
                sexValue = FEMALE;
        }

        SharedPreferences form = this.getActivity().getSharedPreferences(SP_NAME, 0);
        SharedPreferences.Editor editor = form.edit();

        editor.putString(SPKEY_NAME,nameValue);
        editor.putString(SPKEY_LASTNAME,lastnameValue);
        editor.putInt(SPKEY_AGE, ageValue);
        editor.putInt(SPKEY_SEX, sexValue);

        editor.commit();
    }
}
package iguerendiain.siema.cursoandroid.common;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import iguerendiain.siema.cursoandroid.R;

public class NavigationDrawerFragment extends Fragment implements AdapterView.OnItemClickListener {
    private NavigationDrawerCallbacks callbacks;

    private ActionBarDrawerToggle drawerToggle;

    private DrawerLayout drawerLayout;
    private ListView drawerListView;
    private View fragmentContainerView;

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        this.selectItem(i);
    }

    public static interface NavigationDrawerCallbacks {
        void onNavigationDrawerItemSelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.selectItem(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.drawerListView = (ListView) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        this.drawerListView.setOnItemClickListener(this);

        MainActivity.AppSection[] sections = MainActivity.AppSection.values();

        String[] titles = new String[sections.length];
        for (int s=0; s<sections.length; s++){
            titles[s] = this.getString(sections[s].stringId);
        }

        this.drawerListView.setAdapter(
            new ArrayAdapter<String>(
                getActionBar().getThemedContext(),
                R.layout.view_navigation_drawer_item,
                R.id.title,
                titles
            )
        );
        this.drawerListView.setItemChecked(0, true);
        return drawerListView;
    }

    public boolean isDrawerOpen() {
        return this.drawerLayout != null && this.drawerLayout.isDrawerOpen(this.fragmentContainerView);
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        this.fragmentContainerView = this.getActivity().findViewById(fragmentId);
        this.drawerLayout = drawerLayout;

        this.drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        this.drawerToggle = new ActionBarDrawerToggle(
                this.getActivity(),                 /* host Activity */
                this.drawerLayout,                  /* DrawerLayout object */
                R.drawable.ic_drawer,               /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,    /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close    /* "close drawer" description for accessibility */
        ){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!NavigationDrawerFragment.this.isAdded()) {
                    return;
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!NavigationDrawerFragment.this.isAdded()) {
                    return;
                }
            }
        };

        // Defer code dependent on restoration of previous instance state.
        this.drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                NavigationDrawerFragment.this.drawerToggle.syncState();
            }
        });

        this.drawerLayout.setDrawerListener(drawerToggle);
    }

    public void toggle(){
        if (this.isDrawerOpen()){
            this.close();
        }else{
            this.open();
        }
    }

    public void open(){
        this.drawerLayout.openDrawer(NavigationDrawerFragment.this.fragmentContainerView);
    }

    public void close(){
        this.drawerLayout.closeDrawers();
    }

    private void selectItem(int position) {
        if (this.drawerListView != null) {
            this.drawerListView.setItemChecked(position, true);
        }
        if (this.drawerLayout != null) {
            this.drawerLayout.closeDrawer(this.fragmentContainerView);
        }
        if (this.callbacks != null) {
            this.callbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.callbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.callbacks = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.drawerToggle.onConfigurationChanged(newConfig);
    }

    private ActionBar getActionBar() {
        return this.getActivity().getActionBar();
    }
}

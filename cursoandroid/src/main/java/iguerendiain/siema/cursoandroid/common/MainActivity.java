package iguerendiain.siema.cursoandroid.common;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import iguerendiain.siema.cursoandroid.R;
import iguerendiain.siema.cursoandroid.lifecycle.LifeCycleFragment;
import iguerendiain.siema.cursoandroid.ninepatch.NinePatchFragment;
import iguerendiain.siema.cursoandroid.roboguice.RoboguiceFragment;
import iguerendiain.siema.cursoandroid.service.ServiceFragment;
import iguerendiain.siema.cursoandroid.sharedprefs.SharedPrefsFragment;
import iguerendiain.siema.cursoandroid.webservices.WebServicesDetailsFragment;
import iguerendiain.siema.cursoandroid.webservices.WebServicesListFragment;
import roboguice.activity.RoboFragmentActivity;

// Estoy utilizando RoboFragmentActivity solo porque RoboguiceFragment lo requiere.
// Sin embargo, esta Activity podría utilizar FragmentActivity sin cambiar nada más que la clase de la que hereda
public class MainActivity extends RoboFragmentActivity implements WebServicesListFragment.WebServicesListListener, WebServicesDetailsFragment.WebServicesDetailsListener, NavigationDrawerFragment.NavigationDrawerCallbacks {

    public enum AppSection{
        LIFECYCLE(R.string.lifecycle_title, -1),
        NINEPATCH(R.string.ninepatch_title, -1),
        SERVICE(R.string.service_title, -1),
        SHAREDPREFS(R.string.sharedprefs_title, -1),
        WEBSERVICES(R.string.webservices_title, R.menu.videogame_list),
        ROBOGUICE(R.string.roboguice_title, -1);

        public int stringId;
        public int menuId;

        AppSection(int stringId, int menuId){
            this.stringId = stringId;
            this.menuId = menuId;
        }

    }

    private static final String TAG = "CURSO_ANDROID_ACTIVITY_LIFECYCLE";
    private int forcedMenu = -1;
    private Fragment currentFragment;
    private NavigationDrawerFragment navigationDrawer;
    private int currentPosition;
    private String lastTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");
        this.setContentView(R.layout.activity_main);


        this.navigationDrawer = (NavigationDrawerFragment) this.getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        this.navigationDrawer.setUp(R.id.navigation_drawer,(DrawerLayout) this.findViewById(R.id.drawer_layout));
        this.currentPosition = 0;

        this.lastTitle = this.getString(AppSection.values()[this.currentPosition].stringId);
        this.restoreActionBar();

        if (savedInstanceState == null) {
            this.currentFragment = new LifeCycleFragment();
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, this.currentFragment)
                    .commit();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        this.currentPosition = position;

        if (position == AppSection.LIFECYCLE.ordinal()){
            this.currentFragment = new LifeCycleFragment();
        }else if (position == AppSection.NINEPATCH.ordinal()){
            this.currentFragment = new NinePatchFragment();
        }else if (position == AppSection.SERVICE.ordinal()){
            this.currentFragment = new ServiceFragment();
        }else if (position == AppSection.SHAREDPREFS.ordinal()){
            this.currentFragment = new SharedPrefsFragment();
        }else if (position == AppSection.WEBSERVICES.ordinal()){
            this.currentFragment = new WebServicesListFragment(this);
        }else if (position == AppSection.ROBOGUICE.ordinal()){
            this.currentFragment = new RoboguiceFragment();
        }

        this.lastTitle = this.getString(AppSection.values()[this.currentPosition].stringId);
        this.invalidateOptionsMenu();
        this.restoreActionBar();
        this.getSupportFragmentManager().beginTransaction().replace(R.id.container,this.currentFragment).commit();
    }

    private void setMenu(Menu menu){
        int menuId;
        if (this.forcedMenu>0){
            menuId = this.forcedMenu;
            this.forcedMenu = -1;
        }else{
            menuId = AppSection.values()[this.currentPosition].menuId;
        }

        if (menuId > 0){
            this.getMenuInflater().inflate(menuId, menu);
        }else{
            this.getMenuInflater().inflate(R.menu.app_default, menu);
        }
    }

    private void restoreActionBar() {
        ActionBar actionBar = this.getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(this.lastTitle);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.setMenu(menu);
        return true;
    }

    private void goToDetails(){
        this.goToDetails(-1);
    }

    private void goToDetails(long videoGameId){
        if (videoGameId > 0) {
            this.currentFragment = new WebServicesDetailsFragment(this, videoGameId);
        }else{
            this.currentFragment = new WebServicesDetailsFragment(this);
        }

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, this.currentFragment)
                .addToBackStack(null)
                .commit();

        this.forcedMenu = R.menu.videogame_details;
        this.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.navigationDrawer.toggle();
                return true;
            case R.id.action_new_game:
                if (this.currentFragment instanceof WebServicesListFragment){
                    this.goToDetails();
                }
                return true;
            case R.id.action_refresh:
                if (this.currentFragment instanceof WebServicesListFragment){
                    ((WebServicesListFragment)this.currentFragment).reload();
                }
                return true;
            case R.id.action_save_game:
                if (this.currentFragment instanceof WebServicesDetailsFragment){
                    ((WebServicesDetailsFragment)this.currentFragment).saveGame();
                    return true;
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGoToDetails(long videoGameId) {
        this.goToDetails(videoGameId);
    }

    @Override
    public void onGoToList() {
        this.currentFragment = new WebServicesListFragment(this);
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, this.currentFragment)
                .commit();
        this.invalidateOptionsMenu();

    }
}

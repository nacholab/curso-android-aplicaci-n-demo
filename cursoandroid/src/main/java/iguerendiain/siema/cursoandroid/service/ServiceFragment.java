package iguerendiain.siema.cursoandroid.service;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import iguerendiain.siema.cursoandroid.R;

/**
 * Created by capitannerd on 18/03/14.
 */
public class ServiceFragment extends Fragment implements View.OnClickListener {

    private Button startService;
    private Button stopService;
    private Button sendMessage;
    private EditText messageInput;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service, container, false);

        this.startService = (Button) rootView.findViewById(R.id.service_start);
        this.stopService = (Button) rootView.findViewById(R.id.service_stop);
        this.sendMessage = (Button) rootView.findViewById(R.id.service_send_msg);
        this.messageInput = (EditText) rootView.findViewById(R.id.service_msg_input);

        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.startService.setOnClickListener(this);
        this.stopService.setOnClickListener(this);
        this.sendMessage.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent serviceIntent = new Intent(this.getActivity(),ServiceExample.class);
        switch (view.getId()){
            case R.id.service_start:
                this.getActivity().startService(serviceIntent);
                return;
            case R.id.service_stop:
                this.getActivity().stopService(serviceIntent);
                return;
            case R.id.service_send_msg:
                ServiceExample s = ServiceExample.getInstance();
                if (s!=null){
                    ServiceExample.getInstance().sendMessage(this.messageInput.getText().toString());
                }else{
                    Toast.makeText(this.getActivity(),R.string.service_not_running,Toast.LENGTH_LONG).show();
                }
                return;
        }
    }
}
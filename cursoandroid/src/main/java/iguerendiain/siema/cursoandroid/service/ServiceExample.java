package iguerendiain.siema.cursoandroid.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import iguerendiain.siema.cursoandroid.R;

public class ServiceExample extends Service {
    private static final String LOG_TAG = "CursoAndroid-SERVICE";
    private static final String NOTIFICATION_TITLE = "Curso Android";
    private long startedTime;

    private static final int MSG_NOTIFICATION_ID = 4747;
    private static int ONGOING_NOTIFICATION_ID = 1337;
    private static int UPDATE_RATE = 2000;      // En milisegundos

    private ScheduledExecutorService scheduler;
    private boolean running;
    private static ServiceExample instance;
    private Notification.Builder ongoingNotificationBuilder;
    private NotificationManager notificationManager;

    public ServiceExample() {
        super();
        ServiceExample.instance = this;
    }

    public static ServiceExample getInstance(){
        return instance;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "Ejecutándose...");

        if (!this.running){
            this.running=true;
            this.init();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG, "Destruído");
        this.clearOngoingNotification();
        this.scheduler.shutdownNow();
        this.running=false;
        ServiceExample.instance = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void init(){
        this.initNotification();
        this.startedTime = System.currentTimeMillis();
        this.scheduler = Executors.newScheduledThreadPool(1);
        this.scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                long runningTime = System.currentTimeMillis() - ServiceExample.this.startedTime;
                Log.i(LOG_TAG, "Service ejecutándose por " + runningTime + "ms");
                ServiceExample.this.updateNotificationTime(runningTime);
            }
        }, 0, UPDATE_RATE, TimeUnit.MILLISECONDS);
    }

    private void initNotification(){
        this.notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        this.ongoingNotificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(NOTIFICATION_TITLE)
                .setOngoing(true);

        this.showOngoingNotification("Service comenzado...");
    }

    private void updateNotificationTime(long time){
        int seconds = (int) (time / 1000);
        this.showOngoingNotification("Service ejecutándose por " + seconds + " segundos");
    }

    private void showOngoingNotification(String message){
        this.ongoingNotificationBuilder.setContentText(message);
        this.notificationManager.notify(ONGOING_NOTIFICATION_ID,this.ongoingNotificationBuilder.build());
    }

    private void clearOngoingNotification(){
        this.notificationManager.cancel(ONGOING_NOTIFICATION_ID);
    }

    public void sendMessage(String msg) {
        if (this.running){
            Notification.Builder builder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(NOTIFICATION_TITLE)
                    .setOngoing(false)
                    .setContentText("Mensaje recibido: " + msg);

            this.notificationManager.notify(MSG_NOTIFICATION_ID,builder.build());
        }
    }

}

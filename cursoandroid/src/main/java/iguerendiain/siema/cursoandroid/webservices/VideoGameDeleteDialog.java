package iguerendiain.siema.cursoandroid.webservices;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import iguerendiain.siema.cursoandroid.R;

/**
 * Created by capitannerd on 28/03/14.
 */
public class VideoGameDeleteDialog extends DialogFragment {
    public interface VideoGameDeleteDialogListener{
        public void onYes();
    }

    private VideoGameDeleteDialogListener listener;

    public VideoGameDeleteDialog(VideoGameDeleteDialogListener listener){
        super();
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.webservices_delete_video_game_question)
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    VideoGameDeleteDialog.this.listener.onYes();
                }
            })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    VideoGameDeleteDialog.this.dismiss();
                }
            });

        return builder.create();
    }
}

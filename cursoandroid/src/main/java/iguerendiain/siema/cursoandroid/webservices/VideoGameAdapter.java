package iguerendiain.siema.cursoandroid.webservices;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by capitannerd on 24/03/14.
 */
public class VideoGameAdapter extends BaseAdapter {
    private final Context context;
    private List<VideoGameModel> videoGames;

    public VideoGameAdapter(Context context){
        this.videoGames = new ArrayList<VideoGameModel>();
        this.context = context;
    }

    public void setVideoGames(List<VideoGameModel> videoGames){
        this.videoGames = videoGames;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.videoGames.size();
    }

    @Override
    public VideoGameModel getItem(int i) {
        return this.videoGames.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.getItem(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view==null){
            view = new VideoGameListView(this.context,this.getItem(i));
        }else{
            ((VideoGameListView)view).setVideoGame(this.getItem(i));
        }

        return view;
    }
}

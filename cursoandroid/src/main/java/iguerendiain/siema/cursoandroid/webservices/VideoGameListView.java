package iguerendiain.siema.cursoandroid.webservices;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iguerendiain.siema.cursoandroid.R;

/**
 * Created by capitannerd on 24/03/14.
 */
public class VideoGameListView extends RelativeLayout{
    private TextView name;
    private TextView slogan;

    public VideoGameListView(Context context, VideoGameModel videoGame){
        super(context);
        this.init();
        this.setVideoGame(videoGame);
    }

    public VideoGameListView(Context context, AttributeSet attrs, VideoGameModel videoGame) {
        super(context, attrs);
        this.init();
        this.setVideoGame(videoGame);
    }

    public VideoGameListView(Context context, AttributeSet attrs, int defStyle, VideoGameModel videoGame) {
        super(context, attrs, defStyle);
        this.init();
        this.setVideoGame(videoGame);
    }

    private void init(){
        LayoutInflater li = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root = li.inflate(R.layout.videogame_list_item_view,this);

        this.name = (TextView) root.findViewById(R.id.name);
        this.slogan = (TextView) root.findViewById(R.id.slogan);
    }

    public void setVideoGame(VideoGameModel videoGame){
        this.name.setText(videoGame.getName());
        this.slogan.setText(videoGame.getSlogan());
    }


}

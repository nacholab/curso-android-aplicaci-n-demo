package iguerendiain.siema.cursoandroid.webservices;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class VideoGameClient {
    private static final String BASE_URL = "http://192.168.1.12:47/";

    private RestAdapter restAdapter;
    private VideoGameAPI videoGameAPI;
    private Context context;

    public VideoGameClient(Context context){
        this.context = context;
        this.init();
    }

    private void init(){
        if (this.restAdapter == null || this.videoGameAPI == null){
            GsonBuilder jsonParserBuilder = new GsonBuilder();
            jsonParserBuilder.setPrettyPrinting();
            Gson jsonParser = jsonParserBuilder.create();

            GsonConverter jsonParserConverter = new GsonConverter(jsonParser,"UTF-8");

            this.restAdapter = new RestAdapter.Builder()
                    .setEndpoint(BASE_URL)
                    .setConverter(jsonParserConverter)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();

            this.videoGameAPI = this.restAdapter.create(VideoGameAPI.class);
        }
    }

    public VideoGameAPI getAPI(){
        this.init();
        return this.videoGameAPI;
    }

}
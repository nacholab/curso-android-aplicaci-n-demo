package iguerendiain.siema.cursoandroid.webservices;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import iguerendiain.siema.cursoandroid.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class WebServicesListFragment extends Fragment implements Callback<List<VideoGameModel>>, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{
    public interface WebServicesListListener{
        public void onGoToDetails(long videoGameId);
    }

    private ListView videoGameList;
    private VideoGameClient videoGameClient;
    private RelativeLayout loading;
    private final WebServicesListListener listener;

    public WebServicesListFragment(WebServicesListListener listener){
        this.listener = listener;
        this.videoGameClient = new VideoGameClient(this.getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_webservices_list, container, false);
        this.videoGameList = (ListView) rootView.findViewById(R.id.video_game_list);
        this.loading = (RelativeLayout) rootView.findViewById(R.id.loading);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.videoGameList.setAdapter(new VideoGameAdapter(this.getActivity()));
        this.videoGameList.setOnItemClickListener(this);
        this.videoGameList.setOnItemLongClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.loadGames();
    }

    private void loadGames(){
        this.videoGameList.setVisibility(View.GONE);
        this.loading.setVisibility(View.VISIBLE);
        this.videoGameClient.getAPI().getAllVideoGames(this);
    }

    public void reload(){
        this.loadGames();
    }

    private void promptVideoGameDelete(final long id) {
        VideoGameDeleteDialog deleteDialog = new VideoGameDeleteDialog(new VideoGameDeleteDialog.VideoGameDeleteDialogListener() {
            @Override
            public void onYes() {
                WebServicesListFragment.this.deleteVideoGame(id);
            }
        });
        deleteDialog.show(this.getActivity().getFragmentManager(), "");
    }

    private void deleteVideoGame(long id){
        this.videoGameClient.getAPI().deleteVideoGame(id,new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                WebServicesListFragment.this.loadGames();
                Toast.makeText(WebServicesListFragment.this.getActivity(),R.string.webservices_videogame_deleted,Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                WebServicesListFragment.this.loading.setVisibility(View.GONE);
                Toast.makeText(WebServicesListFragment.this.getActivity(),R.string.webservices_error_deleting_videogames,Toast.LENGTH_LONG).show();
            }
        });
    }

    // Métodos pertenecientes a la interfaz Callback
    @Override
    public void success(List<VideoGameModel> videoGameModels, Response response) {
        ((VideoGameAdapter)this.videoGameList.getAdapter()).setVideoGames(videoGameModels);
        this.videoGameList.setVisibility(View.VISIBLE);
        this.loading.setVisibility(View.GONE);
    }

    @Override
    public void failure(RetrofitError error) {
        this.loading.setVisibility(View.GONE);
        Toast.makeText(this.getActivity(),R.string.webservices_error_loading_videogames,Toast.LENGTH_LONG).show();
    }

    // Método de la interfaz onItemClickListener
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        this.listener.onGoToDetails(id);
    }

    // Método de la interfaz onItemLongClickListener
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
        this.promptVideoGameDelete(id);
        return true;
    }

}
package iguerendiain.siema.cursoandroid.webservices;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by capitannerd on 25/03/14.
 */
public interface VideoGameAPI {

    @GET("/videoGame")
    void getAllVideoGames(Callback<List<VideoGameModel>> videoGames);

    @GET("/videoGame/{id}")
    void getVideoGame(@Path("id") Long id, Callback<VideoGameModel> videoGame);

    @POST("/videoGame")
    void createVideoGame(@Body VideoGameModel requestedVideoGame, Callback<VideoGameModel> videoGame);

    @PUT("/videoGame/{id}")
    void updateVideoGame(@Path("id") Long id, @Body VideoGameModel requestedVideoGame, Callback<VideoGameModel> videoGame);

    @DELETE("/videoGame/{id}")
    void deleteVideoGame(@Path("id") Long id, Callback<Void> response);
}

package iguerendiain.siema.cursoandroid.webservices;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import iguerendiain.siema.cursoandroid.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class WebServicesDetailsFragment extends Fragment implements Callback<VideoGameModel>{
    public interface WebServicesDetailsListener{
        public void onGoToList();
    }

    private static final int LOADING_VG = 1;
    private static final int UPDATING_VG = 2;
    private static final int CREATING_VG = 3;

    private WebServicesDetailsListener listener;
    private int action = -1;
    private EditText name;
    private EditText slogan;
    private EditText description;
    private VideoGameClient videoGameClient;
    private VideoGameModel currentVideoGame;


    private RelativeLayout loading;
    private long requestedVideoGameId = -1;

    public WebServicesDetailsFragment(WebServicesDetailsListener listener, long videoGameId){
        this.requestedVideoGameId = videoGameId;
        this.videoGameClient = new VideoGameClient(this.getActivity());
        this.listener = listener;
    }

    public WebServicesDetailsFragment(WebServicesDetailsListener listener){
        this.videoGameClient = new VideoGameClient(this.getActivity());
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_webservices_details, container, false);

        this.loading = (RelativeLayout) rootView.findViewById(R.id.loading);
        this.name = (EditText) rootView.findViewById(R.id.name_input);
        this.slogan = (EditText) rootView.findViewById(R.id.slogan_input);
        this.description = (EditText) rootView.findViewById(R.id.description_input);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.loadGame();
    }

    private void loadGame(){
        if (this.requestedVideoGameId>0) {
            this.action = LOADING_VG;
            this.loading.setVisibility(View.VISIBLE);
            this.videoGameClient.getAPI().getVideoGame(this.requestedVideoGameId, this);
        }
    }

    private void updateUI(){
        this.name.setText(this.currentVideoGame.getName());
        this.slogan.setText(this.currentVideoGame.getName());
        this.description.setText(this.currentVideoGame.getDescription());
    }

    public void saveGame(){
        this.loading.setVisibility(View.VISIBLE);
        VideoGameModel vg = new VideoGameModel();
        vg.setName(this.name.getText().toString());
        vg.setSlogan(this.slogan.getText().toString());
        vg.setDescription(this.description.getText().toString());

        if (this.requestedVideoGameId>0) {
            this.action = UPDATING_VG;
            this.videoGameClient.getAPI().updateVideoGame(this.requestedVideoGameId,vg,this);
        }else{
            this.action = CREATING_VG;
            this.videoGameClient.getAPI().createVideoGame(vg,this);
        }
    }

    // Métodos pertenecientes a la interfaz Callback
    @Override
    public void success(VideoGameModel videoGameModel, Response response) {
        switch (this.action){
            case LOADING_VG:
                this.loading.setVisibility(View.GONE);
                this.currentVideoGame = videoGameModel;
                this.updateUI();
                return;
            case CREATING_VG:
                this.requestedVideoGameId = videoGameModel.getId();
                Toast.makeText(this.getActivity(),R.string.webservices_videogame_created,Toast.LENGTH_LONG).show();
                this.listener.onGoToList();
                return;
            case UPDATING_VG:
                Toast.makeText(this.getActivity(),R.string.webservices_videogame_updated,Toast.LENGTH_LONG).show();
                this.listener.onGoToList();
                return;
        }
    }

    @Override
    public void failure(RetrofitError error) {
        this.loading.setVisibility(View.GONE);
        Toast.makeText(this.getActivity(),R.string.webservices_error_loading_videogame_details,Toast.LENGTH_LONG).show();
    }

}
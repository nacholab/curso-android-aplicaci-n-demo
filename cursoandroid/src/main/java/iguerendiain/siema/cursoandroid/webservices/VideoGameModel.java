package iguerendiain.siema.cursoandroid.webservices;

/**
 * Created by capitannerd on 24/03/14.
 */
public class VideoGameModel {
    private long id;
    private String name;
    private String slogan;
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

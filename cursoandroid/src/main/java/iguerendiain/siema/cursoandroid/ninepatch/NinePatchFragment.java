package iguerendiain.siema.cursoandroid.ninepatch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import iguerendiain.siema.cursoandroid.R;

/**
 * Created by capitannerd on 18/03/14.
 */
public class NinePatchFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ninepatch, container, false);
        return rootView;
    }
}

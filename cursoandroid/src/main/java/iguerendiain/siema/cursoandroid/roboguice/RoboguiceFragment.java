package iguerendiain.siema.cursoandroid.roboguice;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.inject.Inject;

import iguerendiain.siema.cursoandroid.R;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by capitannerd on 18/03/14.
 */
public class RoboguiceFragment extends RoboFragment {
    private static final String SPKEY_NAME = "name";
    private static final String SPKEY_LASTNAME = "lastname";
    private static final String SPKEY_AGE = "age";
    private static final String SPKEY_SEX = "sex";

    private static final int MALE = 0;
    private static final int FEMALE = 1;

    @Inject
    private SharedPreferences savedForm;

    @InjectView(R.id.name_input)
    private EditText name;

    @InjectView(R.id.lastname_input)
    private EditText lastname;

    @InjectView(R.id.age_input)
    private EditText age;

    @InjectView(R.id.sex_input)
    private RadioGroup sex;

    @InjectView(R.id.sex_input_female)
    private RadioButton female;

    @InjectView(R.id.sex_input_male)
    private RadioButton male;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_roboguice, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.restoreData();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.restoreData();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.storeData();
    }

    private void restoreData(){
        if (this.savedForm!=null) {
            String nameValue = this.savedForm.getString(SPKEY_NAME, "");
            String lastnameValue = this.savedForm.getString(SPKEY_LASTNAME, "");
            int ageValue = this.savedForm.getInt(SPKEY_AGE, -1);
            int sexValue = this.savedForm.getInt(SPKEY_SEX, FEMALE);

            this.name.setText(nameValue);
            this.lastname.setText(lastnameValue);
            if (ageValue < 0) {
                this.age.setText("");
            } else {
                this.age.setText(Integer.toString(ageValue));
            }

            switch (sexValue) {
                case FEMALE:
                    this.female.setChecked(true);
                    this.male.setChecked(false);
                    break;
                case MALE:
                    this.female.setChecked(false);
                    this.male.setChecked(true);
                    break;
            }
        }
    }

    private void storeData(){
        if (this.savedForm!=null) {

            String nameValue = this.name.getText().toString();
            String lastnameValue = this.lastname.getText().toString();
            int ageValue;

            try {
                ageValue = Integer.valueOf(this.age.getText().toString());
            } catch (NumberFormatException e) {
                ageValue = -1;
            }

            int sexValue;
            switch (this.sex.getCheckedRadioButtonId()) {
                case R.id.sex_input_female:
                    sexValue = FEMALE;
                    break;
                case R.id.sex_input_male:
                    sexValue = MALE;
                    break;
                default:
                    sexValue = FEMALE;
            }

            SharedPreferences.Editor editor = this.savedForm.edit();

            editor.putString(SPKEY_NAME, nameValue);
            editor.putString(SPKEY_LASTNAME, lastnameValue);
            editor.putInt(SPKEY_AGE, ageValue);
            editor.putInt(SPKEY_SEX, sexValue);

            editor.commit();
        }
    }
}